import { Sequelize } from "sequelize";
import usuariosModel from './models/usuariosModel.js';
import clientesModel from './models/clientesModel.js';

class db {
    constructor(){
        this.sequelize = null;
    }
    
    async inicializarSequelize(dbName,dbUser,dbHost,dbPassword){
        let sequelize = new Sequelize(
            dbName, 
            dbUser, 
            dbPassword, 
            {
                dialect: "postgres", //informar o tipo de banco que vamos utilizar
                host: dbHost, //o host do banco de dados
            }
        );

        usuariosModel.init(sequelize);
        clientesModel.init(sequelize);

        usuariosModel.associate(sequelize);

        await sequelize.sync({ alter: true });
        this.sequelize = sequelize;
    }
}

export default db;