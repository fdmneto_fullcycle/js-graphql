import { GraphQLString, GraphQLNonNull } from "graphql";
import ClienteType from "./typeDef.js";
import Cliente from "../../models/clientesModel.js";


const createCliente ={
    type: ClienteType,
    args: {
        nome: { type: GraphQLString }
    },
    resolve: async (parent, args, context, info) => {
        const cliente = await Cliente.create(args);
        return cliente; 
    }
}

export { createCliente }