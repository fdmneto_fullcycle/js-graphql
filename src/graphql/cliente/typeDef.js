import { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList } from "graphql";
 
const ClienteType = new GraphQLObjectType({
    name: "Cliente",
    fields: () => ({ 
        id: { type: GraphQLInt },
        nome: { type: GraphQLString }
    })
})

export default ClienteType;