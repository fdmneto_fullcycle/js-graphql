import { GraphQLInt, GraphQLList } from "graphql";
import Cliente from "../../models/clientesModel.js";
import ClienteType from "./typeDef.js";


const getCliente = {
    type: ClienteType,
    args: {
        id: { type: GraphQLInt },
    },
    resolve: async (parent, args, context, info) => {
        const cliente = await Cliente.findByPk(args.id);
        return cliente
    }
}

const getTodosClientes = {
    type: new GraphQLList(ClienteType),
    args: {},
    resolve: async (parent, args, context, info) => {
        const todosClientes = await Cliente.findAll();
        return todosClientes
    }
}
export {getCliente, getTodosClientes}