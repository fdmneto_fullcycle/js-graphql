import { GraphQLInt,GraphQLList } from "graphql";
import Usuario from "../../models/usuariosModel.js";
import UsuarioType from "./typeDef.js";


const getUsuario = {
    type: UsuarioType,
    args: {
        id: { type: GraphQLInt },
    },
    resolve: async (parent, args, context, info) => {
        const usuario = await Usuario.findByPk(args.id);
        return usuario;     
    }
}

const getTodosUsuarios = {
    type: new GraphQLList(UsuarioType),
    resolve: async (parent, args, context, info) => {
        const todosUsuarios = await Usuario.findAll({
            include: ["cliente"]
        });
        return todosUsuarios;
    }
}
export {getUsuario, getTodosUsuarios}