import { GraphQLString, GraphQLInt, GraphQLNonNull } from "graphql";
import UsuarioType from "./typeDef.js";
import Usuario from "../../models/usuariosModel.js";


const createUsuario ={
    type: UsuarioType,
    args: {
        nome: { type: GraphQLString },
        cliente_id: { type: GraphQLInt }
    },
    resolve: async (parent, args, context, info) => {
        const usuario = await Usuario.create(args);
        return usuario; 
    }
}

export { createUsuario }