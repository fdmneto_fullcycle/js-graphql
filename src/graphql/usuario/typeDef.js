import { GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList } from "graphql";
import clienteType from '../cliente/typeDef.js'
 
const UsuarioType = new GraphQLObjectType({
    name: "Usuario",
    fields: () => ({ 
        id: { type: GraphQLInt },
        nome: { type: GraphQLString },
        cliente_id: { type: GraphQLInt },
        cliente: {type: clienteType}
    })
})

export default UsuarioType;