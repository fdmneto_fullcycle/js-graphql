import express from 'express';
import usuariosModel from '../models/usuariosModel.js';

const router = express.Router();

router.get('/usuarios', async (request, response) => {
    const usuarios = await usuariosModel.findAll();
    response.json(usuarios);
});

router.get('/usuarios/:id', async (request, response) => {
    const usuario = await usuariosModel.findOne({
        where: {id:request.params.id}
    });

    response.json(usuario);
});

export default router;
