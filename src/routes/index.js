import express from 'express';
import hello from './helloRoute.js';
import usuarios from './usuariosRoute.js';

const routes = (app) => {
  app.route('/').get((_, res) => {
    res.status(200).send({ titulo: 'Curso de node' });
  });

  app.use(
    express.json(),
    hello,
    usuarios,
  );
};

export default routes;