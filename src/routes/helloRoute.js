import express from 'express';

const router = express.Router();

router.get('/hello', (request, response) => {
  response.send('Hello, GraphQL!')
});

export default router;
