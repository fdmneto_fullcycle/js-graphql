
import {Model, DataTypes} from 'sequelize';
 
export default class ClienteModel extends Model {
    static init(sequelize) {
        super.init({
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true
            },
            nome: {
                type: DataTypes.STRING,
                allowNull: true
            },
            createdAt: {
                type: DataTypes.DATE,
                field: "created_at"
            },
            updatedAt: {
                type: DataTypes.DATE,
                field: "updated_at"
            }
        }, 
        {
            sequelize, 
            tableName: "clientes",
            timestamps: true,
            underscored: true,
        });
    }

    static associate(sequelize) {
    }
}
 
