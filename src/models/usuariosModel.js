
import {Model, DataTypes} from 'sequelize';
 
export default class UsuarioModel extends Model {
    static init(sequelize) {
        super.init({
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true
            },
            nome: {
                type: DataTypes.STRING,
                allowNull: true
            },
            cliente_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            createdAt: {
                type: DataTypes.DATE,
                field: "created_at"
            },
            updatedAt: {
                type: DataTypes.DATE,
                field: "updated_at"
            }
        }, 
        {
            sequelize, 
            tableName: "usuarios",
            timestamps: true,
            underscored: true,
        });
    }

    static associate(sequelize) {
        UsuarioModel.belongsTo(sequelize.models.ClienteModel, {
            as: "cliente",
            foreignKey: "cliente_id"
        });
    }
}
 
