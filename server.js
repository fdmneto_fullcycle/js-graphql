import app from './src/app.js'
import db from './src/db.js';
import { graphqlHTTP } from'express-graphql';
import {GraphQLObjectType, GraphQLSchema} from'graphql';
import * as usuarioMutations from'./src/graphql/usuario/mutations.js';
import * as usuarioQuery from'./src/graphql/usuario/query.js';
import * as clienteMutations from'./src/graphql/cliente/mutations.js';
import * as clienteQuery from'./src/graphql/cliente/query.js';
import Usuario from'./src/models/usuariosModel.js';
import Cliente from'./src/models/clientesModel.js';

const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        ...usuarioQuery, 
        ...clienteQuery
    }
})

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: () => ({
        ...usuarioMutations, 
        ...clienteMutations
    })
})

const dbPostgres = new db();

const dbName = 'postgres'; // passar os dados do .env para as constantes
const dbUser = 'postgres';
const dbHost = 'localhost';
const dbPassword = '@N&t0l0c@l'

dbPostgres.inicializarSequelize(dbName,dbUser,dbHost,dbPassword)
.then(() => {
    app.use(
        "/graphql",
        graphqlHTTP({
          schema: new GraphQLSchema({
                        query: Query,
                        mutation: Mutation
                      }),
          graphiql: true
        })
    );
    
    
    const port = 8080;
    app.listen(port, () => {
        console.log(`Running a REST server at http://localhost:${port}`);
        console.log(`Running a GraphQl server at http://localhost:${port}/graphql`);
    })
})
.catch((error) => {
    console.error('Ocorreu um erro ao inicializar o Sequelize');
    throw(error);
})

